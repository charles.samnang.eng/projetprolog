regle(r1, [mange_viande], carnivore).
regle(r2, [dents_pointues, griffes, yeux_en_avant], carnivore).
regle(r3, [mammifere, sabots], ongule).
regle(r4, [mammifere, ruminant], ongule).
regle(r5, [mammifere, carnivore, brun, taches], guepard).
regle(r6, [mammifere, carnivore, brun, raies], tigre).
regle(r7, [ongule, long_cou, longues_pattes, taches], girafe).
regle(r8, [ongule, raies], zebre).
regle(r9, [poils], mammifere).
regle(r10, [donne_lait], mammifere).
regle(r11, [plumes], oiseau).
regle(r12, [vole, pond_oeufs], oiseau).
regle(r13, [nage], aquatique).
regle(r14, [aquatique, ecailles], poisson).
regle(r15, [aquatique, carapace], tortue).
regle(r16, [mammifere, mange_banane], singe).
regle(r17, [mammifere, rongeur, petite_queue], rat).
regle(r18, [mammifere, rongeur, grande_queue], ecureuil).

regle(r20, [combattant,guerrier, sabre, japonais], samourai).
regle(r21, [combat_corps_a_corps], guerrier).
regle(r22, [combattant,guerrier,epee, bouclier], chevalier).
regle(r23, [combattant, mage, boule_de_feu], magicienFeu).
regle(r28, [combattant, mage, boule_de_glace], magicienGlace).
regle(r24, [distance, magie], mage).
regle(r25, [combattant, distance, tire_fleche], archer).
regle(r26, [combattant, guerrier, lance, cheval], cavalier).
regle(r27, [soigne_blessure], soigneur).
regle(r29, [japonais, furtif], ninja).
regle(r30, [silencieux, assassin], furtif).
regle(r31, [combattant, guerrier, main_nu], moine).
regle(r32, [combattant, distance, fusil], fusillier).
regle(r33, [combattant, guerrier, epee, soigneur], paladin).
regle(r34, [mage, soigneur], magicienBlanc).

animal([guepard,tigre,singe,girafe,zebre,oiseau, poisson, tortue, rat, ecureuil]).

classes([samourai,chevalier,paladin,cavalier,magicienFeu,magicienGlace, magicienBlanc, archer,ninja, moine, fusillier]).

/* question demande à l'utilisateur le fait et effectue une action en fonction de la réponse*/
question(Fait):- 
	write("Est-ce que " ), write(Fait), write(" ? (oui/non/""je sais pas"")"), nl, lire_phrase(Reponse), reponse(Reponse, Fait).
	
reponse(Reponse, Fait) :- Reponse == [non], assert(faux(Fait)).
reponse(Reponse, Fait) :- Reponse == [oui], assert(vrai(Fait)).
reponse(Reponse, Fait) :- Reponse == [je, sais, pas], assert(saispas(Fait)).
		
/*	Test effectué pour question(Fait).

	?- question(poils)
	Est-ce que poils ? (oui/non/"je sais pas")
	|: non.
	true .
	faux(poils).
	true.
*/


/* observable(Fait) vérifie si le fait est bien observable ou non.*/
observable(Fait) :- findall(X, regle(_,_,X),L), not(member(Fait,L)).

/*	Test effectué pour observable(Fait).

	?- observable(oiseau).
	false.

	?- observable(raies).
	true.

	?- observable(test).
	true.
*/

/* eval(Fait) vérifie si le fait est vrai ou faux dans la base de données, si il n'existe pas, vérifie si il est observable ou non, si oui, pose la question, sinon evalue les critères du fait*/
eval(Fait) :- faux(Fait), !, fail.
eval(Fait) :- vrai(Fait),!.
eval(Fait) :- observable(Fait),!, question(Fait), vrai(Fait).
eval(Fait) :- regle(_,L,Fait), evalueListe(L).

/* test effectué pour eval(Fait)

	?- eval(oiseau).
	Est-ce que plumes ? (oui/non/"je sais pas")
	|: non.
	Est-ce que vole ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que pond_oeufs ? (oui/non/"je sais pas")
	|: non.

	false.

	?- eval(tortue).
	Est-ce que nage ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que carapace ? (oui/non/"je sais pas")
	|: oui.

	true .
*/

/* evalueListe permet d'évaluer tous les éléments de la liste, si ils sont tous vrai, renvoie vrai.*/
evalueListe([]).
evalueListe([X|L]) :- eval(X), evalueListe(L).

/* test effectué pour evalueListe

	?- evalueListe([poils, mange_viande, plumes]).
	Est-ce que poils ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que mange_viande ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que plumes ? (oui/non/"je sais pas")
	|: non.

	false.

	?- evalueListe([poils, mange_viande, plumes]).
	Est-ce que poils ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que mange_viande ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que plumes ? (oui/non/"je sais pas")
	|: oui.

	true .
*/


/* evalListe permet de vérifier les éléments de la liste 1 a 1, et si le premier est vrai, ça s'arrête et écrit dans la console l'élément.*/
evalListe([X|_]) :- eval(X),!, write(X).
evalListe([_|L]) :- evalListe(L).

/* test effectué pour evalListe

	?- evalListe([singe,oiseau,tortue]).
	Est-ce que poils ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que mange_banane ? (oui/non/"je sais pas")
	|: oui.
	singe
	true.

	?- evalListe([singe,oiseau,tortue]).
	Est-ce que poils ? (oui/non/"je sais pas")
	|: non.
	Est-ce que donne_lait ? (oui/non/"je sais pas")
	|: non.
	Est-ce que plumes ? (oui/non/"je sais pas")
	|: non.
	Est-ce que vole ? (oui/non/"je sais pas")
	|: non.
	Est-ce que nage ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que carapace ? (oui/non/"je sais pas")
	|: oui.
	tortue
	true.

*/

/* cherche permet de chercher quel élément de la liste est vrai*/
cherche() :- suppData(), animal(L),write("Choisissez un animal parmi la liste suivante."),nl, write(L),nl, evalListe(L).

/* test effectué pour cherche()

	?- cherche().
	Choisissez un animal parmi la liste suivante.
	[guepard,tigre,singe,girafe,zebre,oiseau,poisson,tortue,rat,ecureuil]
	Est-ce que poils ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que mange_viande ? (oui/non/"je sais pas")
	|: non.
	Est-ce que dents_pointues ? (oui/non/"je sais pas")
	|: non.
	Est-ce que donne_lait ? (oui/non/"je sais pas")
	|: non.
	Est-ce que mange_banane ? (oui/non/"je sais pas")
	|: non.
	Est-ce que sabots ? (oui/non/"je sais pas")
	|: non.
	Est-ce que ruminant ? (oui/non/"je sais pas")
	|: non.
	Est-ce que plumes ? (oui/non/"je sais pas")
	|: non.
	Est-ce que vole ? (oui/non/"je sais pas")
	|: non.
	Est-ce que nage ? (oui/non/"je sais pas")
	|: non.
	Est-ce que rongeur ? (oui/non/"je sais pas")
	|: oui.
	Est-ce que petite_queue ? (oui/non/"je sais pas")
	|: non.
	Est-ce que grande_queue ? (oui/non/"je sais pas")
	|: oui.
	ecureuil
	true.
	
*/

/* cherche2() fait pareil que cherche() mais avec la liste des classes.*/
cherche2() :- suppData(), classes(L), write("Choisissez une classe parmi la liste suivante."), nl, write(L), nl, evalListe(L).

/* lire_phrase et divise permettent de lire la réponse de l'utilisateur*/
lire_phrase(Liste):-
	read(Phrase),
	name(Phrase,Lphrase),
	divise(Lphrase,Liste).


divise(Chaine,[MotL|Liste]) :- 
		append(Mot,[32|Reste],Chaine),
		name(MotL,Mot),
		divise(Reste,Liste).
divise(DernierMot,[DernierMotL]):-
		name(DernierMotL,DernierMot).


/*suppData supprime la base de données*/
suppData() :- retractall(vrai(X)), retractall(faux(X)), retractall(saispas(X)).
